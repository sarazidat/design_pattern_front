package com.example.design_pattern_front.service

import com.example.design_pattern_front.dto.DtoItemMenu
import com.example.design_pattern_front.dto.DtoRestaurant
import com.example.design_pattern_front.models.ClientModel
import com.example.design_pattern_front.models.CommandeModel
import com.example.design_pattern_front.models.ItemMenuModel
import com.example.design_pattern_front.models.RestaurantModel



import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    @GET("clients")
    suspend fun getClient(): Response<MutableList<ClientModel>>

    @GET("commandes")
    suspend fun getCommande(): Response<MutableList<CommandeModel>>

    @GET("restaurants")
    suspend fun getRestaurant(): Response<MutableList<RestaurantModel>>

    @GET("itemmenus/{id}")
    suspend fun getItemmenuById(@Path("num") num : Int): Response<ItemMenuModel>

    @POST("restaurants")
    suspend fun createRestaurant(@Body post: DtoRestaurant): Response<RestaurantModel>

    @POST("itemmenu")
    suspend fun createItemmenu(@Body post: DtoItemMenu): Response<ItemMenuModel>


}