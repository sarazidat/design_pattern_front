package com.example.design_pattern_front

import android.content.Context
import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.design_pattern_front.models.RestaurantModel

class RestaurantListAdapter (private var restaurants: ArrayList<RestaurantModel>, private val context : Context) {
    fun updateRestaurants(newUsers: List<RestaurantModel>) {
        newUsers.forEach() {
            Log.d("updateUsers", it.id.toString())
        }
        restaurants.clear()
        restaurants.addAll(newUsers)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : RestaurantViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_layout,parent,false)
        return RestaurantViewHolder(view)
    }

    override fun getItemCount():Int {
        return restaurants.size
    }
    override fun onBindViewHolder(holder: RestaurantViewHolder, position: Int) {
        holder.bind(restaurants[position])
    }

    class RestaurantViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val restaurantName = view.nom
        private val restaurantAdresse = view.adresse
        private val restaurantCategorie = view.categorie


        fun bind(country: RestaurantModel) {
            restaurantName.text = country.nom + " " + country.nom
            restaurantAdresse.text = country.adresse
            restaurantCategorie.text = country.categorie

        }
    }
}