package com.example.design_pattern_front.models

data class CommandeModel(
    val id: Int? = null,
    val idClient: Int? = null,
    val idItems: Array<Int>? = null,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CommandeModel

        if (id != other.id) return false
        if (idClient != other.idClient) return false
        if (idItems != null) {
            if (other.idItems == null) return false
            if (!idItems.contentEquals(other.idItems)) return false
        } else if (other.idItems != null) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id ?: 0
        result = 31 * result + (idClient ?: 0)
        result = 31 * result + (idItems?.contentHashCode() ?: 0)
        return result
    }
}
