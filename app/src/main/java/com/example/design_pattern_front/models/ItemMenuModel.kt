package com.example.design_pattern_front.models

data class ItemMenuModel(
    val id : Int? = null,
    val idRestau: Int? = null,
    val nom : String? = null,
    val description : String? = null,
    val prix: Double? = null,
    val categorie: String? = null,
)
