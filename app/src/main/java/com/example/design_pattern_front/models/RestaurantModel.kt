package com.example.design_pattern_front.models

data class RestaurantModel(
    val id: Int? = null,
    val nom: String? = null,
    val adresse: String? = null,
    val categorie: String? = null,
)
