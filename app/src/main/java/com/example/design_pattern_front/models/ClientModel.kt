package com.example.design_pattern_front.models

data class ClientModel(
    val id: Int? = null,
    val userName: String? = null,
    val email: String? = null,
    val password: String? = null,
    val address : String? = null,
)
