package com.example.design_pattern_front

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.coroutines.Dispatchers


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this).get(ListViewModel::class.java)
        viewModel.refresh()

        restaurantsList.apply {
            layoutManager = LinearLayoutManager(this.context,
                RecyclerView.VERTICAL, false)
            adapter = restaurantsAdapter
        }
        observeViewModel()
    }
    private fun observeViewModel() {

        viewModel.users.observe(this, Observer {
            it?.let {
                it.forEach {
                    Log.d("Each Item", it.toString());
                }
                //loading_view.visibility = View.VISIBLE
                restaurantsAdapter.updateUsers(it)
            }
        })
   /* private fun executeCall() {
        launch(Dispatchers.Main) {
            try {
                val response = ApiClient.apiService.getItemmenuById(1)

                if (response.isSuccessful && response.body() != null) {
                    val content = response.body()
//do something
                } else {
                    Toast.makeText(
                        this@MainActivity,
                        "Error Occurred: ${response.message()}",
                        Toast.LENGTH_LONG
                    ).show()
                }

            } catch (e: Exception) {
                Toast.makeText(
                    this@MainActivity,
                    "Error Occurred: ${e.message}",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }*/
}